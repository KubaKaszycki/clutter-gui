# How to contribute?

Although this library is intended to be small, sized perfectly for games (and
thus providing only basic widgets), any contribution, even if complex, is
welcome!

## Which file can I send?

Anything will help :-)

The best format is a **Git diff**. Here is how to generate one:

	git diff                                      # Currently uncommited changes
	git diff commit-id1 commit-id2 ... commit-idn # Particular commit(s)
	git diff file1 file2 ... filen                # Particular file(s)

The command will output colored text, if the target is a terminal. Review the
changes carefully. If there are some that you don't want to share, you will
later cross them out of the file.

Now, forward this to a file, named `<something>.diff`. Open it in your
favourite editor, and look at the changes once again.

## Who do I send stuff to?

### Method 1 (preferred)

You need to have M4 and Autoconf set up properly, all needed macros in `m4/`
and generated `aclocal.m4`. Run this:

	autoconf -t m4_define:'$1=$2' | grep '^bug_report_url=' | sed 's|^.*=||g'

### Method 2 (manual)

Open [configure.ac](configure.ac). Find a line which looks so (`<...>` will be
the bug report address):

	m4_define([bug_report_url],[<...>])

Then, copy the `<...>` to your clipboard.

### What next?

This will reveal an e-mail address (currently) or an URL.

 * If it is an e-mail address, send a mail with the patch as an attachment OR
   in the text, please, choose one
 * If it is an URL, go there and fill an issue. Bugzilla allows one to specify
   the type of attachment as a patch, then it can display this side-by-side.

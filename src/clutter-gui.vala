using Clutter;

/**
 * ''ClutterGUI'' is a project aiming to provide GUI for Clutter based
 * applications.
 * 
 * @author Jakub Kaszycki
 */
[CCode (gir_namespace = "ClutterGUI", gir_version = "1.0")]
namespace ClutterGUI
{
  // Some utilities

  /**
   * Calculates the average of two colors.
   * 
   * @since 0.1.0
   */
  public Color
  color_average (Color a, Color b)
  {
    return { (a.red + b.red) / 2, (a.green + b.green) / 2,
      (a.blue + b.blue) / 2, (a.alpha + b.alpha) / 2 };
  }
}

using Clutter;

namespace ClutterGUI
{
  public class Component : Actor
  {
    /**
     * Determines whether the component is active. An active component
     * responds to events.
     * 
     * @since 1.0
     */
    public bool active { get; set; default = true; }

    /**
     * Set to {{{true}}} when left mouse button is pressed while over this
     * component, and to {{{false}}} when it is released.
     * 
     * @since 1.0
     */
    public bool _clicked { get; protected set; default = false; }

    /**
     * Set to {{{true}}} when mouse enters the component and to {{{false}}}
     * when it leaves.
     * 
     * @since 1.0
     */
    public bool _hover { get; protected set; default = false; }

    public
    Component ()
    {
      opacity = 255;
      reactive = true;

      button_press_event.connect (() => {
        if (active)
          _clicked = true;
        return false;
      });
      button_release_event.connect (() => {
        if (active)
          {
            if (_hover && _clicked)
              click ();
            _clicked = false;
          }
        return false;
      });
      enter_event.connect (() => {
        if (active)
          _hover = true;
        return false;
      });
      leave_event.connect (() => {
        if (active)
          _hover = false;
        return false;
      });
    }

    public virtual signal void
    resize ()
    {
    }

    /**
     * Sent when the mouse is pressed over the component and then released,
     * still over the component.
     * 
     * @since 0.1.0
     */
    public virtual signal void
    click ()
    {
    }
  }
}

using Clutter;

namespace ClutterGUI
{
  public class Label : Component
  {
    public Text text_actor;

    public string text
      {
        get
          {
            return text_actor.text;
          }
        set
          {
            text_actor.text = value;
          }
      }

    public string font
      {
        get
          {
            return text_actor.font_name;
          }
        set
          {
            text_actor.font_name = value;
          }
      }

    public Color color
      {
        get
          {
            return text_actor.color;
          }
        set
          {
            text_actor.color = value;
          }
      }

    public
    Label (string? text = "", string? font = null,
           Color? color = { 0, 0, 0, 255 })
    {
      base ();

      text_actor = new Text ();
      text_actor.x = 0;
      text_actor.y = 0;
      text_actor.opacity = 255;
      add_child (text_actor);

      if (text == null)
        text = "";
      if (color == null)
        color = { 0, 0, 0, 255 };

      this.text = text;
      this.font = font;
      this.color = color;
    }

    public override void
    get_preferred_width (float h, out float minw, out float natw)
    {
      text_actor.get_preferred_width (h, out minw, out natw);
    }

    public override void
    get_preferred_height (float w, out float minh, out float nath)
    {
      text_actor.get_preferred_height (w, out minh, out nath);
    }
  }
}

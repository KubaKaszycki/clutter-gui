/* clutter-gui.vala
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Clutter;
using GLib;

namespace ClutterGUI
{
  public class ToggleButton : Button
  {
    public bool selected { get; set; default = false; }

    public
    ToggleButton (string? text = "")
    {
      base (text);
    }

    public override void
    click ()
    {
      selected = !selected;
      toggle (selected);
    }

    public virtual signal void
    toggle (bool val)
    {
    }

    public override Color
    permute_color (Color x)
    {
      var base_perm = base.permute_color (x);
      return selected
        ? color_average (base_perm, click_color)
        : base_perm;
    }
  }
}

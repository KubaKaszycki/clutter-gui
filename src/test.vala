using Clutter;
using ClutterGUI;

int
main (string[] args)
{
    {
      var optctx = new OptionContext ();
      optctx.add_group (Clutter.get_option_group ());
      try
        {
          optctx.parse (ref args);
        }
      catch (OptionError e)
        {
          stderr.printf ("Option parsing error: %s\n", e.message);
          return 2;
        }
    }

  var stage = new Stage ();
  stage.set_size (512, 512);
  stage.background_color = { 0, 0, 255, 255 };
  stage.destroy.connect (main_quit);
  stage.show ();

  float y = 10;

  var button = new Button ("Button");
  button.font_color = { 255, 0, 0, 255 };
  button.resize ();
  button.orig_x = 10;
  button.orig_y = y;
  button.active_color = { 225, 225, 225, 255 };
  button.inactive_color = { 190, 190, 190, 255 };
  button.hover_color = { 245, 245, 245, 255 };
  button.click_color = { 110, 110, 190, 255 };
  button.opacity = 255;
  button.click.connect (() => {
    stdout.printf ("Button clicked!\n");
    stdout.flush ();
  });
  button.update_color ();
  stage.add_child (button);

  y += button.height;
  y += 10;

  var toggle = new ToggleButton ("ToggleButton");
  toggle.font_color = { 0, 255, 0, 255 };
  toggle.resize ();
  toggle.orig_x = 10;
  toggle.orig_y = y;
  toggle.active_color = { 225, 225, 225, 255 };
  toggle.inactive_color = { 190, 190, 190, 255 };
  toggle.hover_color = { 245, 245, 245, 255 };
  toggle.click_color = { 110, 110, 190, 255 };
  toggle.opacity = 255;
  toggle.click.connect (() => {
    stdout.printf ("Toggle clicked!\n");
    stdout.flush ();
  });
  toggle.toggle.connect ((val) => {
    stdout.printf ("Toggled: %s\n", val ? "on" : "off");
    stdout.flush ();
  });
  toggle.update_color ();
  stage.add_child (toggle);

  y += toggle.height;
  y += 10;

  var label = new Label ("Label");
  label.color = { 255, 255, 0, 255 };
  label.font = "Monospace Bold 50";
  label.resize ();
  label.set_position (10, y);
  label.click.connect (() => {
    stdout.printf ("Label clicked!\n");
    stdout.flush ();
  });
  stage.add_child (label);

  Clutter.main ();

  return 0;
}

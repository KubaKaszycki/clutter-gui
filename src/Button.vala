/* Button.vala - simple button implementation
 *
 * Copyright (C) 2017 Jakub Kaszycki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Clutter;

namespace ClutterGUI
{
  /**
   * Simple button. Can be pressed, releases itself just after releasing the
   * mouse button.
   * 
   * @since 1.0
   */
  public class Button : Component
  {
    /**
     * Color used when the button is active.
     * 
     * @see update_color
     * @since 1.0
     */
    public Color active_color { get; set; }

    /**
     * Color used when the mouse is over the button.
     * 
     * @see update_color
     * @since 1.0
     */
    public Color hover_color { get; set; }

    /**
     * Color used when the button is pressed.
     * 
     * @see update_color
     * @since 1.0
     */
    public Color click_color { get; set; }
    
    /**
     * Color used when the button is inactive.
     * 
     * @see update_color
     * @since 1.0
     */
    public Color inactive_color { get; set; }

    /**
     * Text on this button.
     * 
     * @see label
     * @since 1.0
     */
    public string text
      {
        get
          {
            return label.text;
          }
        set
          {
            label.text = value;
          }
      }

    /**
     * Font used for text.
     * 
     * @see text
     * @since 1.0
     */
    public string font
      {
        get
          {
            return label.font;
          }
        set
          {
            label.font = value;
          }
      }

    /**
     * Color of text.
     * 
     * @see text
     * @since 1.0
     */
    public Color font_color
      {
        get
          {
            return label.color;
          }
        set
          {
            label.color = value;
          }
      }

    /**
     * Label used for rendering text.
     * 
     * @since 1.0
     */
    public Label label;

    private float _orig_x;
    public float orig_x
      {
        get
          {
            return _orig_x;
          }
        set
          {
            _orig_x = value;
            if (_hover)
              {
                if (_clicked)
                  {
                    x = value + 1;
                  }
                else
                  {
                    x = value - 1;
                  }
              }
            else
              {
                x = value;
              }
          }
      }
    private float _orig_y;
    public float orig_y
      {
        get
          {
            return _orig_y;
          }
        set
          {
            _orig_y = value;
            if (_hover)
              {
                if (_clicked)
                  {
                    y = value + 1;
                  }
                else
                  {
                    y = value - 1;
                  }
              }
            else
              {
                y = value;
              }
          }
      }

    /**
     * Sets up a button.
     * 
     * @param text text to be visible on the button
     * 
     * @since 1.0
     */
    public
    Button (string? text = "")
    {
      base ();

      if (text == null)
        text = "";

      label = new Label (text);

      opacity = 255;
      label.opacity = 255;

      _clicked = false;
      _hover = false;
      active = true;

      orig_x = x;
      orig_y = y;

      button_press_event.connect (() => {
        if (active)
          {
            _clicked = true;
            x = orig_x + 1;
            y = orig_y + 1;
            update_color ();
          }
        return false;
      });
      button_release_event.connect (() => {
        if (active)
          {
            _clicked = false;
            x = orig_x - 1;
            y = orig_y - 1;
            update_color ();
          }
        return true;
      });
      enter_event.connect (() => {
        if (active)
          {
            _hover = true;
            x = orig_x - 1;
            y = orig_y - 1;
            update_color ();
          }
        return true;
      });
      leave_event.connect (() => {
        if (active)
          {
            _hover = false;
            _clicked = false;
            x = orig_x;
            y = orig_y;
            update_color ();
          }
        return true;
      });

      add_child (label);

      update_color ();
    }

    /**
     * {@inheritDoc}
     */
    public override void
    resize ()
    {
      // Center the label
      float natw, nath;
      float maxw = width - 10, maxh = height - 10;
      label.get_preferred_size (null, null, out natw, out nath);

      natw = natw > maxw ? maxw : natw;
      nath = nath > maxh ? maxh : nath;

      label.width = natw;
      label.height = nath;
      label.x = (width - natw) / 2;
      label.y = (height - nath) / 2;
    }

    /**
     * {@inheritDoc}
     */
    public override void
    get_preferred_width (float h, out float minw, out float natw)
    {
      float label_minw;
      float label_natw;
      label.get_preferred_width (h - 10, out label_minw, out label_natw);

      minw = label_minw + 10;
      natw = label_natw + 10;
    }

    /**
     * {@inheritDoc}
     */
    public override void
    get_preferred_height (float w, out float minh, out float nath)
    {
      float label_minh;
      float label_nath;
      label.get_preferred_height (w - 10, out label_minh, out label_nath);

      minh = label_minh + 10;
      nath = label_nath + 10;
    }

    /**
     * Updates the color of the button based on current state.
     * 
     * @since 1.0
     */
    public virtual void
    update_color ()
    {
      if (active)
        {
          if (_hover)
            {
              if (_clicked)
                {
                  background_color = permute_color (click_color);
                }
              else
                {
                  background_color = permute_color (hover_color);
                }
            }
          else
            {
              background_color = permute_color (active_color);
            }
        }
      else
        {
          background_color = permute_color (inactive_color);
        }
    }

    /**
     * Permutes the color based on current state.
     * 
     * @param x color to permute
     * @return modified color
     * 
     * @since 1.0
     */
    public virtual Color
    permute_color (Color x)
    {
      return { x.red, x.green, x.blue, x.alpha };
    }
  }
}
